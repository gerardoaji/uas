import React, {Component} from 'react';
import {Container, Header, Content, List} from 'native-base';

import Anime from './components/Anime';

export default class MyAnimeList extends Component {
    animes = [
        'Boruto',
        'Boku no Hero Academia',
        'Dragon Ball Super',
        'Shingki no Bahamut'
    ]

    constructor(){
        super();
    }

    render(){
        return (
            <Container>
                <Header/>
                <Content>
                    <List>
                        {this.animes.map((anime, key)=> <Anime key={key} anime={anime}/>)}
                    </List>
                </Content>
            </Container>
        )
    }
}